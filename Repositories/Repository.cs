using System;
using System.Data;
using MySql.Data.MySqlClient;
using MiamAPI.Models;
using System.Collections.Generic;

namespace MiamAPI.Repositories
{
    public abstract class Repository
    {
        public MySqlConnection sqlConnection {get;set;}

        protected int getLastInsertId() {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select LAST_INSERT_ID()";
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            int id = -1;
            while(reader.Read()) {
                id = Convert.ToInt32(reader.GetValue(0));
            }
            sqlConnection.Close();
            return id;
        }
    }
}