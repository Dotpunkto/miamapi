using System.Collections.Generic;
using MySql.Data.MySqlClient;
using MiamAPI.Models;
using System.Data;
using System;

namespace MiamAPI.Repositories
{
    public class RecipeRepository: Repository
    {

        public RecipeRepository(string connectionString) {
            sqlConnection = new MySqlConnection(connectionString);
        }

        public int add(RecipeIngredients recipe) {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"insert into Recipes (name, category, picture, score) select '{recipe.Name}', '{recipe.Category}', '{recipe.Picture}', '{recipe.Score}' "
                            + $"where not exists(select name from Recipes where name = '{recipe.Name}')";
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            int rowInserted = cmd.ExecuteNonQuery();
            sqlConnection.Close();

            if (rowInserted == 0) {
                return -1;
            }
            return getLastInsertId();
        }

        public RecipeIngredients getRecipeByName(string name) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"Select * from Recipes where name like '{name}'";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader(); 
            RecipeIngredients recipe = new RecipeIngredients();
            while(reader.Read()) {
                recipe.ID = (int)reader.GetValue(0);
                recipe.Name = (string)reader.GetValue(1);
                recipe.Category = (string)reader.GetValue(2);
                recipe.Picture = (string)reader.GetValue(3);
                recipe.Score = (int)reader.GetValue(4);
            }         
            sqlConnection.Close(); 
            return recipe;
        }

        public List<Ingredient> getIngredientsWithRecipeName(RecipeIngredients recipe) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"SELECT * FROM miam.ingredients i LEFT JOIN miam.recipeingredient r2 ON i.ID = r2.ingredientID "
                            + $"LEFT JOIN miam.recipes r ON r2.ingredientID = r.ID where r2.recipeID = {recipe.ID}";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Ingredient> ingredients = new List<Ingredient>();
            while(reader.Read()) {
                Ingredient ingredient = new Ingredient{
                    ID = (int)reader.GetValue(0),
                    Name = (string)reader.GetValue(1)
                };
                ingredients.Add(ingredient);
            }         
            sqlConnection.Close(); 
            return ingredients;
        }

        public List<RecipeIngredients> getRecipeByIngredient(string ingredients) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"SELECT * FROM miam.recipes r LEFT JOIN miam.recipeingredient r2 ON r.ID = r2.recipeID "
                            + $"LEFT JOIN miam.ingredients i ON r2.ingredientID = i.ID where i.name in ({ingredients})";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<RecipeIngredients> recipes = new List<RecipeIngredients>();
            while(reader.Read()) {
                RecipeIngredients recipe = new RecipeIngredients{
                    ID = (int)reader.GetValue(0),
                    Name = (string)reader.GetValue(1),
                    Category = (string)reader.GetValue(2),
                    Picture = (string)reader.GetValue(3),
                    Score = (int)reader.GetValue(4)
                };
                recipes.Add(recipe);
            }         
            sqlConnection.Close(); 
            return recipes;
        }

        public int getRecipeID(string name) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"Select id from Recipes where name like '{name}'";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader(); 
            var id = -1;
            while(reader.Read()) {
                id = Convert.ToInt32(reader.GetValue(0));
            }         
            sqlConnection.Close(); 
            return id;
        }
    }
}