using System;

using System.Data;
using MySql.Data.MySqlClient;
using MiamAPI.Models;
using System.Collections.Generic;

namespace MiamAPI.Repositories
{
    class IngredientRepository: Repository
    {

        public IngredientRepository(string connectionString) {
            sqlConnection = new MySqlConnection(connectionString);
        }

        public int add(string name) {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"insert into Ingredients (name) select '{name}' where not exists(select name from Ingredients where name = '{name}')";
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            int rowInserted = cmd.ExecuteNonQuery();
            sqlConnection.Close();

            if (rowInserted == 0) {
                return -1;
            }
            return getLastInsertId();
        }

        public List<Ingredient> getByName(string name) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"Select * from Ingredients where name like '{name}%'";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<Ingredient> ingredients = new List<Ingredient>();  
            while(reader.Read()) {
                Ingredient ingredient = new Ingredient {
                    ID = (int)reader.GetValue(0),
                    Name = (string)reader.GetValue(1)
                };
                ingredients.Add(ingredient);
            }            
            sqlConnection.Close(); 
            return ingredients;
        }

        public int getIngredientID(string name) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"Select id from Ingredients where name = '{name}'";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            int ID = -1; 
            while(reader.Read()) {
                ID = (int)reader.GetValue(0);
            }            
            sqlConnection.Close(); 
            return ID;
        }

    }
}