using System.Security.Cryptography.X509Certificates;
using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace MiamAPI.Repositories
{
    public class RecipeIngredientsRepository: Repository
    {

        public RecipeIngredientsRepository(string connectionString) {
            sqlConnection = new MySqlConnection(connectionString);
        }

        public void add(int recipeID, int ingredientID) {
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"insert into recipeingredient (recipeID, ingredientID) values ('{recipeID}', '{ingredientID}') ";
            cmd.Connection = sqlConnection;
            sqlConnection.Open();
            int rowInserted = cmd.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public bool isExist(int recipeID, int ingredientID) {
            MySqlCommand cmd = new MySqlCommand();  
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"Select id from recipeingredient where recipeID = '{recipeID}' and ingredientID = '{ingredientID}'";
            cmd.Connection = sqlConnection;  
            sqlConnection.Open();
            MySqlDataReader reader = cmd.ExecuteReader();
            
            if(reader.Read()) {
                sqlConnection.Close();
                return false;
            }
            sqlConnection.Close();
            return true;
        }

    }
}