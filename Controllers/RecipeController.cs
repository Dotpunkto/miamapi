using System;
using System.Collections.Generic;
using MiamAPI.Models;
using MiamAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace MiamAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class RecipeController
    {

        private RecipeRepository recipeRepository;
        private IngredientRepository ingredientRepository;
        private RecipeIngredientsRepository recipeIngredientsRepository;

        public RecipeController() {
            string connectionString = "Server=localhost;Port=3306;Database=Miam;Uid=root;Pwd=rootroot";
            recipeRepository = new RecipeRepository(connectionString);
            ingredientRepository = new IngredientRepository(connectionString);
            recipeIngredientsRepository = new RecipeIngredientsRepository(connectionString);
        }

        [HttpPost]
        public void addRecipe(RecipeIngredients recipe) {
            int recipeID;
            recipeID = recipeRepository.add(recipe);
            List<int> ingredientsID = new List<int>();
            foreach (var Igredient in recipe.Igredients)
            {
                int index = ingredientRepository.add(Igredient.Name);
                if (index == -1) {
                    ingredientsID.Add(ingredientRepository.getIngredientID(Igredient.Name));
                } else {
                    ingredientsID.Add(index);
                }
            }
            
            if (recipeID == -1) {
                recipeID = recipeRepository.getRecipeID(recipe.Name);
            }

            foreach (var ingredientID in ingredientsID) {
                if(recipeIngredientsRepository.isExist(recipeID, ingredientID)) {
                    recipeIngredientsRepository.add(recipeID, ingredientID);
                }
            }
        }

        [HttpGet]
        public RecipeIngredients getRecipeByName(string name) {
            RecipeIngredients recipe = recipeRepository.getRecipeByName(name);
            recipe.Igredients = recipeRepository.getIngredientsWithRecipeName(recipe);
            return recipe;
        }

        [HttpGet]
        public List<RecipeIngredients> getRecipeByIngredients(string ingredients) {
            List<RecipeIngredients> recipes = recipeRepository.getRecipeByIngredient(ingredients);
            foreach (var recipe in recipes)
            {
                recipe.Igredients = recipeRepository.getIngredientsWithRecipeName(recipe);
            }
            return recipes;
        }
    }
}