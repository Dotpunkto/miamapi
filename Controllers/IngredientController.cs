using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using MiamAPI.Models;
using MiamAPI.Repositories;
using System.Configuration;

namespace MiamAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class IngredientController
    {
        private IngredientRepository ingredientRepsitory;

        public IngredientController() {
            ingredientRepsitory = new IngredientRepository("Server=localhost;Port=3306;Database=Miam;Uid=root;Pwd=rootroot");
        }

        [HttpPost]
        public void addIngredient(Ingredient ingredient) {
            ingredientRepsitory.add(ingredient.Name);
        }

        [HttpGet]
        public List<Ingredient> getIngredient(string name) {
            return ingredientRepsitory.getByName(name);
        }
        
    }
}