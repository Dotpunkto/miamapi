CREATE DATABASE Miam;

CREATE TABLE Miam.Ingredients (
    ID INTEGER NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    PRIMARY KEY (ID)
) ENGINE=InnoDB;

CREATE TABLE Miam.Recipes (
    ID INTEGER NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    category varchar(255) NOT NULL,
    picture varchar(255),
    score INTEGER,
    PRIMARY KEY (ID)
) ENGINE=InnoDB;

CREATE TABLE Miam.RecipeIngredient (
    ID INTEGER NOT NULL AUTO_INCREMENT,
    recipeID INTEGER,
    ingredientID INTEGER,
    PRIMARY KEY (ID),
    KEY recipe_FK (recipeID),
    CONSTRAINT recipe_FK FOREIGN KEY (recipeID) REFERENCES Recipes (ID),
    KEY ingredient_FK (ingredientID),
    CONSTRAINT ingredient_FK FOREIGN KEY (ingredientID) REFERENCES Ingredients (ID)
) ENGINE=InnoDB;
