namespace MiamAPI.Models
{
    public class Ingredient
    {
        public int ID {get;set;}
        public string Name {get;set;}
    }
}