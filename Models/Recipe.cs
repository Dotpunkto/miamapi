using System.Collections.Generic;

namespace MiamAPI.Models
{
    public class Recipe
    {
        public int ID {get;set;}
        public string Name {get;set;}
        public string Category {get;set;}
        public string Picture {get;set;}
        public int Score {get;set;}
    }
}