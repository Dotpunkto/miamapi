using System.Collections.Generic;

namespace MiamAPI.Models
{
    public class RecipeIngredients
    {
        public int ID {get;set;}
        public string Name {get;set;}
        public string Category {get;set;}
        public string Picture {get;set;}
        public int Score {get;set;}
        public List<Ingredient> Igredients {get;set;} = new List<Ingredient>();
    }
}